import time, random
from locust import HttpUser, task, between

from faker import Faker

fake = Faker()

class QuickstartUser(HttpUser):
    wait_time = between(1, 2.5)

    def on_start(self):
        self.login()

    def on_stop(self):
        self.logout()

    def createUser(self):
        password= fake.password(8, 64)
        fake.unique.email()
        self.client.post("/register", json={"name":fake.unique.name(), "email":email, "password":password, "password_confirmation":password}) #"date":fake.unix_time(end_datetime=

    def login(self):
        password= fake.password(8, 64)
        email = fake.unique.email()
        self.client.post("/register", json={"name":fake.unique.name(), "email":email, "password":password, "password_confirmation":password}) 
        self.client.post("/login", json={"email":email, "password":password})

    def logout(self):
        self.client.post("/logout")

    @task
    def show_all_fields(self):
        self.client.get("/fields")

    @task
    def create_field(self):
        self.client.post("/fields", json={"field":fake.text(25), "date":fake.unix_time(end_datetime="+15y", start_datetime='+1s'), "user_id":fake.random.randint(1,4)}) #"date":fake.unix_time(end_datetime='+30y', start_datetime='+1s'

    @task
    def showFieldsPaginator(self):
        for page in range(10):
            self.client.get(f"/fields?sortedBy=field&direction=asc&field=teste&page={page}&pageSize=20", name="/fields/page?=")
            #time.sleep(1)

    @task
    def showId(self):
        #field_id = random.randint(11600,11689)
        #with self.client.get(f"/fields/{field_id}", name="/fields/id", catch_response=True) as response:
        response = self.client.get("/fields")
        fields = response.json()['data']
        if len(fields)>0:
            field = random.randint(1, len(fields))
            fieldId = fields[field-1]['id']
            self.client.get(f"/fields/{fieldId}", name="/fields/id")

    @task
    def updateId(self):
        response = self.client.get("/fields")
        fields = response.json()['data']
        if len(fields)>0:
            field = random.randint(1, len(fields))
            fieldId = fields[field-1]['id']
            self.client.put(f"/fields/{fieldId}", json={"field":fake.text(25), "date":fake.unix_time(end_datetime="+15y", start_datetime='+1s')}, name="/fields/id")
        #field_id = random.randint(11600,11689)
        #with self.client.put(f"/fields/{field_id}", json={"field":fake.text(25), "date":fake.unix_time(end_datetime="+15y", start_datetime='+1s')}, name="/fields/id", catch_response=True) as response:#"date":fake.unix_time(end_datetime='+30y', start_datetime='+1s'
           
        
    @task
    def deleteId(self):
        response = self.client.post("/fields", json={"field":fake.text(25), "date":fake.unix_time(end_datetime="+15y", start_datetime='+1s')}, name="/fields/id")
        fieldId = response.json()['data']['id']
        self.client.delete(f"/fields/{fieldId}", name="/fields/id")

