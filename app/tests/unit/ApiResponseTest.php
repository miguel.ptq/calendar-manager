<?php

namespace Tests\App\Exceptions;

use TestCase;
use App\Models\Field;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use App\Traits\ApiResponse;
use Illuminate\Support\Facades\Hash;

class ApiResponseTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $user = User::latest()->where('name', 'like', '%' . 'test' . '%')->first();
        if (!$user) {
            $user = User::factory()->create(
                [
                    'name' => 'test',
                    'email' => 'test@example.com',
                    'password' => Hash::make('test')
                ]
            );
            /*  var_dump($user);
             die; */
        }
        $this->actingAs($user);
        var_dump($user->id);
    }
    use ApiResponse;
    /**
     * Undocumented function
     *test the structure and the response on delete
     * @return void
     */
    public function testReturnSuccessNoData()
    {
        $result = $this->successResponse(null, Response::HTTP_OK);

        $data = $result->getData();

        $this->assertInstanceOf(JsonResponse::class, $result);
        $this->assertObjectHasAttribute('status', $data);
        $this->assertEquals('success', $data->status);
        $this->assertObjectNotHasAttribute('data', $data);
        $this->assertObjectNotHasAttribute('meta', $data);
        $this->assertEquals(Response::HTTP_OK, $result->getStatusCode());
    }
    /**
     * Undocumented function
     *test the structure of an error
     * @return void
     */
    public function testReturnSuccessWhenItsAError()
    {
        $examples = [
            'status' => Response::HTTP_NOT_FOUND,
            'message' => 'Field Not Found'
        ];
        $result = $this->errorResponse('Field Not Found', $examples['status']);

        $data = $result->getData();

        $this->assertInstanceOf(JsonResponse::class, $result);
        $this->assertObjectHasAttribute('status', $data);
        $this->assertEquals('error', $data->status);
        $this->assertObjectNotHasAttribute('data', $data);
        $this->assertObjectHasAttribute('response', $data);
        $this->assertObjectHasAttribute('code', $data->response);
        $this->assertEquals($examples['status'], $data->response->code);
        $this->assertObjectHasAttribute('message', $data->response);
        $this->assertEquals($examples['message'], $data->response->message);
        $this->assertEquals(Response::HTTP_NOT_FOUND, $result->getStatusCode());
    }
    /**
     * Undocumented function
     *test the structure of the response on create
     * @return void
     */
    public function testReturnSuccessWithDataOnCreate()
    {
        /*$user = User::factory()->create();
        $this->actingAs($user);*/

        $field = Field::factory()->create();

        $result = $this->successResponse($field, Response::HTTP_CREATED);
        $body = $result->getData();
        $data = $body->data;

        $this->assertInstanceOf(JsonResponse::class, $result);
        $this->assertObjectHasAttribute('status', $body);
        $this->assertEquals('success', $body->status);
        $this->assertObjectHasAttribute('data', $body);
        //var_dump($field->getAttributes()); die;
        foreach ($field->getAttributes() as $key => $value) {
            $this->assertObjectHasAttribute($key, $data);
            $this->assertEquals($field->{$key}, $data->{$key});
        }



        $this->assertEquals(Response::HTTP_CREATED, $result->getStatusCode());
    }
    /**
     * Undocumented function
     *test structure of the response on get{id} and update{id}
     * @return void
     */
    public function testReturnSuccessWithDataOnGetUpdate()
    {
        /*$user = User::factory()->create();
        $this->actingAs($user);*/

        $field = Field::factory()->create();

        $result = $this->successResponse($field);
        $body = $result->getData();
        $data = $body->data;
        //Verify if the $result is a JsonResponse
        $this->assertInstanceOf(JsonResponse::class, $result);
        //Verify if there is a attribute status
        $this->assertObjectHasAttribute('status', $body);
        //Verify is the response on the attribute status is equal to success
        $this->assertEquals('success', $body->status);
        //Verify if there is a attribute data
        $this->assertObjectHasAttribute('data', $body);
        //var_dump($field->getAttributes()); die;
        //For each attribute of the object field verify if the attribute exists on $data and verify if the values are the same
        foreach ($field->getAttributes() as $key => $value) {
            $this->assertObjectHasAttribute($key, $data);
            $this->assertEquals($field->{$key}, $data->{$key});
        }
        $this->assertEquals(Response::HTTP_OK, $result->getStatusCode());
    }
}
