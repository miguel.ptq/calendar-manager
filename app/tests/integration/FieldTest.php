<?php

use App\Models\Field;
use Illuminate\Http\Response;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class ExampleTest extends TestCase
{

    /**
     * A basic test example.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $user = User::latest()->where('name', 'like', '%' . 'test' . '%')->first();
        if (!$user) {
            $user = User::factory()->create(
                [
                    'name' => 'test',
                    'email' => 'test@example.com',
                    'password' => Hash::make('test')
                ]
            );
            /*  var_dump($user);
             die; */
        }
        $this->actingAs($user);
        var_dump($user->id);
    }
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testReturnField()
    {
        /* $user = User::factory()->create();
        $this->actingAs($user); */
        $field = Field::factory()->create();
        $this->get("fields/{$field->id}", []);
        $this->seeStatusCode(Response::HTTP_OK);
        $this->seeJsonEquals(
            [
                'status' => 'success',
                'data' =>
                [
                    'id' => $field->id,
                    'field' => $field->field,
                    'date' => $field->date,
                    'created_at' => $field->created_at,
                    'updated_at' => $field->updated_at,
                    'user_id' => $field->user_id
                ]
            ]
        );
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function InvalidIdError404()
    {
        $this->get('/fields/this-is-invalid')
            ->seeStatusCode(Response::HTTP_NOT_FOUND);
        $this->seeJsonStructure(
            [
                'status',
                'response' => [
                    'code',
                    'message'
                ]
            ]
        );
    }
    /**
     * Sends the error 404 if the endpoint is invalid
     *
     * @return void
     */
    public function InvalidEndPointError404()
    {
        $this->get('/this-is-invalid')->seeStatusCode(Response::HTTP_NOT_FOUND);
        $this->seeJsonStructure(
            [
                'status',
                'response' => [
                    'code',
                    'message'
                ]
            ]
        );
    }
    /**
     * Sends the error 404 when the id doesn't exist when trying the endpoint get{id}
     *
     * @return void
     */
    public function testFailShowInvalidId()
    {
        /* $user = User::factory()->create();
        $this->actingAs($user); */

        $this->get('/fields/999999999');
        $this->seeStatusCode(Response::HTTP_NOT_FOUND);
        $this->seeJsonStructure(
            [
                'status',
                'response' => [
                    'code',
                    'message'
                ]
            ]
        );
    }
    /**
     * Test the creation of a field, after created verifies if the creation exists on the db
     *
     * @return void
     */
    public function testCreateField()
    {
        /* $user = User::factory()->create();
        $this->actingAs($user); */

        $parameters = [
            'field' => 'tests123',
            'date' => time() + 10
        ];
        $this->post("fields", $parameters, []);
        $this->seeStatusCode(Response::HTTP_CREATED);
        $data = $this->response->getData(true);
        $this->assertArrayHasKey('data', $data);
        $this->seeJson($parameters);
        $this->seeInDatabase('fields', $parameters);
    }
    /**
     * When give the wrong parameters it shouldn't create a field
     *
     * @return void
     */
    public function testShouldNotCreateField()
    {
        /* $user = User::factory()->create();
        $this->actingAs($user); */

        $parameters = [
            'field_Test' => 'tests123',
            'date_test' => time() + 10
        ];
        $this->post("fields", $parameters, []);
        $this->seeStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJsonStructure(
            [
                'status',
                'response' => [
                    'code',
                    'message',
                ]
            ]
        );
    }
    /**
     * When the required parameters are not filled on create, it sends a error
     *
     * @return void
     */
    public function testValidateRequiredFields()
    {
        /* $user = User::factory()->create();
        $this->actingAs($user); */

        $this->post("fields", [], ['Accept' => 'application/json']);
        $this->seeStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJsonStructure(
            [
                'status',
                'response' => [
                    'code',
                    'message'
                ]
            ]
        );
    }
    /**
     * Test the update of a field, after updated verifies if the field was update, by checking if the new data exists on the db and the previous data isn't anymore on the db
     *
     * @return void
     */
    public function testUpdateField()
    {
        /* $user = User::factory()->create();
        $this->actingAs($user); */

        $field = Field::factory()->create();
        $parameters = [
            'field' => 'testes123455',
            'date' => time() + 5
        ];
        $this->put("fields/{$field->id}", $parameters, []);
        $this->seeStatusCode(Response::HTTP_OK);
        $this->seeJsonStructure(
            [
                'status',
                'data' =>
                [
                    'id',
                    'field',
                    'date',
                    'created_at',
                    'updated_at'
                ]
            ]
        );
        $this->seeInDatabase(
            'fields',
            [
                'field' => 'testes123455',
                'date' => time() + 5
            ]
        )
            ->notSeeInDatabase(
                'fields',
                [
                    'field' => $field->name,
                    'date' => $field->date
                ]
            );
    }
    /**
     * This test verifies the structure of an error when trying to update a invalid field (field id doesn't exists)
     *
     * @return void
     */
    public function testFailUpdateInvalidId()
    {
        /* $user = User::factory()->create();
        $this->actingAs($user); */

        $this->put('/fields/999999999');
        $this->seeStatusCode(Response::HTTP_NOT_FOUND);
        $this->seeJsonEquals(
            [
                'status' => 'error',
                'response' =>
                [
                    'code' => Response::HTTP_NOT_FOUND,
                    'message' => 'Field Not Found'
                ]
            ]
        );
    }
    /**
     * Verify the structure of a get
     *
     * @return void
     */
    public function testShowAll()
    {
        /* $user = User::factory()->create();
        $this->actingAs($user); */

        $this->get("fields", []);
        $this->seeStatusCode(Response::HTTP_OK);
        $this->seeJsonStructure(
            [
                'status',
                'data' =>
                [
                    '*' =>
                    [
                        'field',
                        'date',
                        'created_at',
                        'updated_at',
                    ]
                ],
                'meta' =>
                [
                    'currentPage',
                    'fistPage',
                    'lastPage',
                    'itemsPerPage',
                    'totalFields',
                    'links' =>
                    [
                        'nextPageURL',
                        'prevPageUrl',
                        'firstPageUrl',
                        'lastPageUrl',
                        'path',
                    ]
                ]
            ]
        );
    }
    /**
     *
     * Verify if sends a error if the pageSize is higher then 50, and checks the structure of the error
     *
     * @return void
     */
    public function testShouldNotReturnAllFields()
    {
        /* $user = User::factory()->create();
        $this->actingAs($user); */

        $this->get("fields?pageSize=75", [], []);
        $this->seeStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJsonStructure(
            [
                'status',
                'response' => [
                    'code',
                    'message'
                ]
            ]
        );
    }
    /**
     * This test verifies the structure of a get when given all filters
     *
     * @return void
     */
    public function testShowAllWithFilters()
    {
        /*  $user = User::factory()->create();
         $this->actingAs($user); */

        $this->get("fields?sortedBy=field&direction=asc&field=teste&page=5&pageSize=20", []);
        $this->seeStatusCode(Response::HTTP_OK);
        $this->seeJsonStructure(
            [
                'status',
                'data' =>
                [
                    '*' =>
                    [
                        'field',
                        'date',
                        'created_at',
                        'updated_at',
                    ]
                ],
                'meta' =>
                [
                    'currentPage',
                    'fistPage',
                    'lastPage',
                    'itemsPerPage',
                    'totalFields',
                    'links' =>
                    [
                        'nextPageURL',
                        'prevPageUrl',
                        'firstPageUrl',
                        'lastPageUrl',
                        'path',
                    ]
                ]
            ]
        );
    }
    /**
     * This test creates a field and then delete it, and check if the structure of the response, it should only have status
     *
     * @return void
     */
    public function testDeleteField()
    {
        /* $user = User::factory()->create();
        $this->actingAs($user); */

        $field = Field::factory()->create();
        //var_dump($field);
        $this->delete("fields/{$field->id}", [], []);

        //$this->delete("fields/14", [], []);
        $this->seeStatusCode(Response::HTTP_OK);
        $this->notSeeInDatabase('fields', ['id' => $field->id]);
        $this->seeJsonStructure(
            [
                'status'
            ]
        );
    }
    /**
     * This test verifies the structure of an error when trying to delete a invalid field (field id doesn't exists)
     *
     * @return void
     */
    public function testDeleteInvalidFieldReturn404()
    {
        /* $user = User::factory()->create();
        $this->actingAs($user); */

        $this->delete('/fields/999999999');
        $this->seeStatusCode(Response::HTTP_NOT_FOUND);
        $this->seeJsonStructure(
            [
                'status',
                'response' => [
                    'code',
                    'message'
                ]
            ]
        );
    }
    /**
     * This test should check if the length of field is too long, if so, its sends a error, in this test checks if the structure of the error
     *
     * @return void
     */
    public function testValidateIfFieldIsTooLong()
    {
        /* $user = User::factory()->create();
        $this->actingAs($user); */

        foreach ($this->getValidationTestData() as $test) {
            $method = $test['method'];
            $test['data']['field'] = str_repeat('a', 256);

            $this->{$method}(
                $test['url'],
                $test['data'],
                ['Accept' => 'application/json']
            );

            $this->seeStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);
            $this->seeJsonStructure(
                [
                    'status',
                    'response' => [
                        'code',
                        'message'
                    ]
                ]
            );
        }
    }
    /**
     * Test if the length of the field is just long enough
     *
     * @return void
     */
    public function testValidateIfFieldIsLongEnough()
    {
        /* $user = User::factory()->create();
        $this->actingAs($user); */

        foreach ($this->getValidationTestData() as $test) {
            $method = $test['method'];
            $test['data']['field'] = str_repeat('a', 255);

            $this->{$method}(
                $test['url'],
                $test['data'],
                ['Accept' => 'application/json']
            );
            $this->seeStatusCode($test['status']);
            $this->seeInDatabase('fields', $test['data']);
        }
    }
    /**
     * Test if it the response if a error, checking the structure. This error happens if the date is less then today
     *
     * @return void
     */
    public function testValidateIncorrectDateIfIsLessThenToday()
    {
        /* $user = User::factory()->create();
        $this->actingAs($user); */

        foreach ($this->getValidationTestData() as $test) {
            $method = $test['method'];
            $test['data']['date'] = time() - 5;

            $this->{$method}(
                $test['url'],
                $test['data'],
                ['Accept' => 'application/json']
            );

            $this->seeStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);

            $this->seeJsonStructure(
                [
                    'status',
                    'response' => [
                        'code',
                        'message'
                    ]
                ]
            );
        }
    }
    /**
     * This test checks if the date is higher then now
     *
     * @return void
     */
    public function testValidateCorrectDateIfIsHigherThenToday()
    {
        /* $user = User::factory()->create();
        $this->actingAs($user); */

        foreach ($this->getValidationTestData() as $test) {
            $method = $test['method'];
            $test['data']['date'] = time() + 8;

            $this->{$method}(
                $test['url'],
                $test['data'],
                ['Accept' => 'application/json']
            );
            $this->seeStatusCode($test['status']);
            $this->seeInDatabase('fields', $test['data']);
        }
    }
    /**
     * This test checks if the when given an invalid route on update, if it sends a HTTP_NOT_FOUND
     *
     * @return void
     */
    public function testUpdateInvalidRoute()
    {
        /* $user = User::factory()->create();
        $this->actingAs($user); */

        $this->put('/fields/this-is-invalid')
            ->seeStatusCode(Response::HTTP_NOT_FOUND);
    }
    /**
     * This test checks if the when given an invalid route on delete, if it sends a HTTP_NOT_FOUND
     *
     * @return void
     */
    public function testDeleteInvalidRoute()
    {
        /* $user = User::factory()->create();
        $this->actingAs($user); */

        $this->delete('/fields/this-is-invalid')
            ->seeStatusCode(Response::HTTP_NOT_FOUND);
    }
    /**
     * This test checks if if its updating only the fillable fields
     *
     * @return void
     */
    public function testUpdatesOnlyFillableFields()
    {
        /* $user = User::factory()->create();
        $this->actingAs($user); */

        $field = Field::factory()->create();

        $this->put(
            "fields/{$field->id}",
            [
                'field' => 'titulo123',
                'test' => 'kekw.'
            ],
            ['Accept' => 'application/json']
        );
        $this
            ->seeStatusCode(Response::HTTP_OK)
            ->seeJson(
                [
                    'id' => $field->id,
                    'field' => 'titulo123'
                ]
            )
            ->seeInDatabase(
                'fields',
                [
                    'field' => 'titulo123'
                ]
            );
        $content = json_decode($this->response->getContent(), true);
        $this->assertArrayHasKey('data', $content);

        $data = $content['data'];

        $this->assertArrayHasKey('created_at', $data);
        $this->assertEquals($field->created_at, $data['created_at']);
        $this->assertArrayHasKey('updated_at', $data);
        $this->assertEquals(time(), $data['updated_at']);
    }


    private function getValidationTestData()
    {
        $field = Field::factory()->create();
        return [
            //create
            [
                'method' => 'post',
                'url' => '/fields',
                'status' => Response::HTTP_CREATED,
                'data' => [
                    'field' => 'test1234',
                    'date' =>  time() + 1
                ]
            ],
            //update
            [
                'method' => 'put',
                'url' => "/fields/{$field->id}",
                'status' => Response::HTTP_OK,
                'data' => [
                    'field' => $field->field,
                    'date' =>  time() + 1
                ]
            ]
        ];
    }
}
