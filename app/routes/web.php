<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get(
    '/',
    function () use ($router) {
        return $router->app->version();
    }
);

$router->group(
    ['prefix' => ''],
    function () use ($router) {
        $router->get(
            '/fields',
            [
                'uses' => 'FieldController@showAllFields'
            ]
        );

        $router->get(
            '/fields/{field_id}',
            [
                'uses' => 'FieldController@showField'
            ]
        );

        $router->post(
            '/fields',
            [
                'uses' => 'FieldController@createField'
            ]
        );

        $router->delete(
            '/fields/{field_id}',
            [
                'uses' => 'FieldController@destroyField'
            ]
        );

        $router->put(
            '/fields/{field_id}',
            [
                'uses' => 'FieldController@updateField'
            ]
        );

        $router->post(
            '/register',
            'AuthController@register'
        );

        $router->post(
            '/login',
            'AuthController@login'
        );

        $router->post(
            '/me',
            'AuthController@me'
        );

        $router->post(
            '/logout',
            'AuthController@logout'
        );

        $router->post(
            '/refresh',
            'AuthController@refresh'
        );

        //Generate App_key
        /*  $router->get(
            '/key',
            function () {
                return \Illuminate\Support\Str::random(32);
            }
        ); */
        //Generate IV_key
        /* $router->get(
            '/iv',
            function () {
                return bin2hex(random_bytes(8));
            }
        ); */
    }
);
