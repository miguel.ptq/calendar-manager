<?php

/**
 * Auth Controller
 *
 * PHP version 7.3.27
 *
 * @category PHP
 * @package  PHP_CodeSniffer
 * @author   Miguel Queiroz <miguel.queiroz.int@gmail.com>
 * @license  http://url.com MIT
 * @link     http://pear.php.net/package/PHP_CodeSniffer
 */

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Traits\Encryptable;
use Illuminate\Http\Client\Response as ClientResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Response;
//use IlluminateSupportFacadesCookie;
//use Symfony\Component\HttpFoundation\Cookie;
use Illuminate\Support\Facades\Cookie;

use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    use Encryptable;

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(
            'auth',
            [
                'except' => [
                    'login', 'register'
                ]
            ]
        );
    }

    public function register(Request $request)
    {
        //$request->email = $this->encryptEmail($request->input('email'));
        $this->validate(
            $request,
            [
                'name' => 'required|string',
                'email' => 'required|max:255|email',
                'password' => 'required|confirmed|min:8|max:64'
            ]
        );
        $email_encrypt = $this->encryptAttribute($request['email']);
        $check_email = User::where('email', '=', $email_encrypt)->first();
        if ($email_encrypt == ($this->encryptAttribute($check_email['email']))) {
            return $this->errorResponse(
                [
                    'email' => [
                        'This email has already been taken!'
                    ]
                ],
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        $user = new User;
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));

        $user->save();

        return $this->successResponse($user, Response::HTTP_CREATED);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @param  Request $request request of the login
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        //validate incoming request
        $this->validate(
            $request,
            [
                'email' => 'required|email',
                'password' => 'required|string',
            ]
        );

        $credentials = request(['email', 'password']);
        $credentials['email'] = $this->encryptAttribute($credentials['email']);
        if (!$token = Auth::attempt($credentials)) {
            return $this->errorResponse(
                'Invalid Credentials!',
                Response::HTTP_UNAUTHORIZED
            );
        }

        $cookie = Cookie::make('jwt', $token, 60); //name, value, expires in 1 hour like the default tokens TTL

        return $this->successResponse(
            new User(
                [
                    'name' => Auth::user()->name,
                    'email' => Auth::user()->email
                ]
            )
        )->withCookie($cookie);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return $this->successResponse(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $cookie = Cookie::forget('jwt');
        Auth::logout();

        return $this->successResponse(null)->withCookie($cookie);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        $token = Auth::refresh();
        $cookie = Cookie::create('jwt', $token, 60);


        return $this->successResponse(
            new User(
                [
                    'name' => Auth::user()->name,
                    'email' => Auth::user()->email
                ]
            )
        )->withCookie($cookie);
    }
}
