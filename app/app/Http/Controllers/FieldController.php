<?php

namespace App\Http\Controllers;

use App\Models\Field;
use App\Models\User;
use App\Http\Resources\FieldCollection;
use Illuminate\Http\Client\Response as ClientResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class FieldController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * This function will list all the fields with filters
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function showAllFields(Request $request)
    {
        $rules = [
            'pageSize' => 'int|min:10|max:50',
            'direction' => 'nullable|in:asc,desc|alpha',
            'sortedBy' => 'in:field,date,id',
            'field' => 'max:255'
        ];
        $this->validate($request, $rules);
        $direction = request('direction', 'desc');
        $sortedBy = request('sortedBy', 'id');
        $field = request('field');
        $pageSize = request('pageSize', 10);

        $collection = Field::where('user_id', '=', Auth::user()->id)
            ->orderBy($sortedBy, $direction);
        if ($field) {
            $collection->where('field', 'like', '%' . $field . '%');
        }
        $collection = $collection->paginate($pageSize)
            ->appends(
                [
                    'sortedBy' => $sortedBy,
                    'direction' => $direction,
                    'field' => $field,
                    'pageSize' => $pageSize
                ]
            );

        return new FieldCollection($collection);
    }

    /**
     * This function will create a field
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function createField(Request $request)
    {
        $rules = [
            'field' => 'required|max:255',
            'date' => 'filled|integer|min:' . (time() + 1)
        ];
        $this->validate($request, $rules);
        $field = request('field');
        $date = request('date');
        $fields = new Field;
        $fields->field = $field;
        $fields->date = $date;
        $fields->user_id = Auth::id();


        $fields->save();
        return $this->successResponse($fields, Response::HTTP_CREATED);
    }
    /**
     * This function is going to list the info of a field
     *
     * @param [integer] $field_id
     * @return JsonResponse
     */
    public function showField($field_id)
    {
        //$field = User::findOrFail(Auth::id())->fields()->findOrFail($field_id);
        $field = Field::where('user_id', '=', Auth::user()->id)
            ->findOrFail($field_id);

        /*$field = Field::where('id', $field_id)->get();
        return $this->successResponse($field);*/
        return $this->successResponse($field);
    }

    /**
     * This function will update the fields that the user gave
     *
     * @param Request $request
     * @param [integer] $field_id
     * @return JsonResponse
     */
    public function updateField(Request $request, $field_id)
    {
        $rules = [
            'field' => 'max:255',
            'date' => 'filled|integer|min:' . (time() + 1)
        ];

        $this->validate($request, $rules);

        $fields = Field::where('user_id', '=', Auth::user()->id)
            ->findOrFail($field_id);
        foreach ($rules as $key => $value) {
            if ($request->has($key)) {
                $fields->{$key} = $request->input($key);
            }
        }
        if ($fields->isClean()) {
            return $this->successResponse($fields);
        }

        $fields->save();

        return $this->successResponse($fields);
    }

    /**
     * This function will delete a field
     *
     * @param [integer] $field_id
     * @return JsonResponse
     */
    public function destroyField($field_id)
    {
        $field = Field::where('user_id', '=', Auth::user()->id)
            ->findOrFail($field_id);
        $field->delete();

        return $this->successResponse(null);
    }
}
