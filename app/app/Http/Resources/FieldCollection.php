<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class FieldCollection extends ResourceCollection
{
    public function __construct($resource)
    {
        $this->pagination = [
            'currentPage' => $resource->currentPage(),
            'fistPage' => 1,
            'lastPage' => $resource->lastPage(),
            'itemsPerPage' => $resource->perPage(),
            'totalFields' => $resource->total(),
            'links' => [
                'nextPageURL' => $resource->nextPageUrl(),
                'prevPageUrl' => $resource->previousPageUrl(),
                'firstPageUrl' => $resource->url(1),
                'lastPageUrl' => $resource->url($resource->lastPage()),
                'path' => $resource->path()
            ]
        ];

        $resource = $resource->getCollection();

        parent::__construct($resource);
    }

    public function toArray($request)
    {
        return [
            'status' => 'success',
            'data' => $this->collection,
            'meta' => $this->pagination
        ];
    }
}
