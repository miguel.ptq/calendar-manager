<?php

namespace App\Exceptions;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;

use App\Traits\ApiResponse;
use Throwable;

class Handler extends ExceptionHandler
{
    use ApiResponse;
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        #return parent::render($request, $exception);
        #$rendered = parent::render($request, $exception);


        if ($exception instanceof HttpException) {
            $errorCode = $exception->getStatusCode();
            $errorMessage = Response::$statusTexts[$errorCode];
            if ($errorCode==Response::HTTP_NOT_FOUND) {
                return $this->errorResponse('Invalid Endpoint', $errorCode);
            }
            return $this->errorResponse($errorMessage, $errorCode);
        }

        if ($exception instanceof ModelNotFoundException) {
            //$model = strtolower(class_basename($exception->getModel()));
            return $this->errorResponse(
                //"{$model} not found",
                'Field Not Found',
                //"Does not exist any instance of {$model} with a given id",
                Response::HTTP_NOT_FOUND
            );
        }

        if ($exception instanceof AuthorizationException) {
            return $this->errorResponse(
                $exception->getMessage(),
                Response::HTTP_FORBIDDEN
            );
        }

        if ($exception instanceof AuthenticationException) {
            return $this->errorResponse(
                $exception->getMessage(),
                Response::HTTP_UNAUTHORIZED
            );
        }

        if ($exception instanceof ValidationException) {
            $errors = $exception->validator->errors()->getMessages();
            return $this->errorResponse(
                $errors,
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (env('APP_DEBUG', true)) {
            return parent::render($request, $exception);
        }

        return $this->errorResponse(
            "Unexpected error. Try later!",
            Response::HTTP_INTERNAL_SERVER_ERROR
        );
    }
}
