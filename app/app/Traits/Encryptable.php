<?php

namespace App\Traits;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Crypt;

trait Encryptable
{
    /**
     * If the attribute is in the encryptable array
     * then decrypt it.
     *
     * @param  $key
     *
     * @return $value
     */
    protected $cipher = "AES-128-CBC";
    public function getAttribute($key)
    {
        $value = parent::getAttribute($key);
        if (in_array($key, $this->encryptable) && $value !== '') {
            $value = $this->decryptAttribute($value);
        }
        return $value;
    }
    /**
     * If the attribute is in the encryptable array
     * then encrypt it.
     *
     * @param $key
     * @param $value
     */
    public function setAttribute($key, $value)
    {
        if (in_array($key, $this->encryptable)) {
            $value = $this->encryptAttribute($value);
        }
        return parent::setAttribute($key, $value);
    }
    /**
     * When need to make sure that we iterate through
     * all the keys.
     *
     * @return array
     */
    public function attributesToArray()
    {
        $attributes = parent::attributesToArray();
        foreach ($this->encryptable as $key) {
            if (isset($attributes[$key])) {
                $attributes[$key] = $this->decryptAttribute($attributes[$key]);
            }
        }
        return $attributes;
    }
    /**
     * Encrpyt a attribute using openssl
     *
     * @param string $value
     * @return string
     */
    public function encryptAttribute($value)
    {
        $ciphertext_raw = openssl_encrypt(
            $value,
            $this->cipher,
            env('APP_KEY'),
            $options = OPENSSL_RAW_DATA,
            env('APP_IV')
        );
        $hmac = hash_hmac(
            'sha256',
            $ciphertext_raw,
            env('APP_KEY'),
            $as_binary = true
        );
        $ciphertext = base64_encode(env('APP_IV') . $hmac . $ciphertext_raw);
        return $ciphertext;
    }
    /**
     * Decrypt Attribute using openssl
     *
     * @param [string] $value
     * @return string
     */
    public function decryptAttribute($value)
    {
        $c = base64_decode($value);
        $ivlen = openssl_cipher_iv_length($this->cipher);
        $hmac = substr($c, $ivlen, $sha2len = 32);
        $ciphertext_raw = substr($c, $ivlen + $sha2len);
        $original_plaintext = openssl_decrypt(
            $ciphertext_raw,
            $this->cipher,
            env('APP_KEY'),
            $options = OPENSSL_RAW_DATA,
            env('APP_IV')
        );
        $calcmac = hash_hmac(
            'sha256',
            $ciphertext_raw,
            env('APP_KEY'),
            $as_binary = true
        );
        if (hash_equals($hmac, $calcmac)) { //PHP 5.6+ timing attack safe comparison
            return $original_plaintext;
        }
    }
}
