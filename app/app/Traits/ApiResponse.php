<?php

namespace App\Traits;

use Illuminate\Http\Response;

trait ApiResponse
{

    /**
     * This returns successful response in json
     *
     * @param array $data
     * @param integer $statusCode
     * @return JsonResponse
     */
    public function successResponse($data, $statusCode = Response::HTTP_OK)
    {
        if (!$data) {
            return response()->json(
                [
                    'status' => 'success'
                ],
                $statusCode
            );
        }

        return response()->json(
            [
                'status' => 'success',
                'data' => $data
            ],
            $statusCode
        );
    }
    /**
     * This returns a error response in json
     *
     * @param [string]  $errorMessage
     * @param [integer] $statusCode
     * @return JsonResponse
     */
    public function errorResponse($errorMessage, $statusCode)
    {
        return response()->json(
            [
                'status' => 'error',
                'response' =>  [
                    'code' => $statusCode,
                    'message' => $errorMessage
                ]
            ],
            $statusCode
        );
    }
}
