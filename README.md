# Calendar Manager

In order to accomplish the task presented bellow you will need the following tools:

 - A cool and functional Code Editor or IDE (we like VSCode, but it's your choice)
 - Docker: [Windows](https://desktop.docker.com/win/stable/Docker%20Desktop%20Installer.exe) ([WSL2](https://docs.microsoft.com/en-us/windows/wsl/install-win10) installation, you will need it), [Mac OS](https://desktop.docker.com/mac/stable/Docker.dmg) and [Linux](https://docs.docker.com/engine/install/ubuntu/)
 - Git: [Windows](https://git-scm.com/download/win), [Mac OS](https://git-scm.com/download/mac) and [Linux](https://git-scm.com/download/linux)

After having everything setup, clone this repo to your local machine and run the command:

> docker-compose up --build -d

This will create a container named "swk-challenge-php", after the container creation you should be able to access the service via [localhost:795](http://localhost:795), if the returns an error maybe the container isn't fully started yet, check the logs with:

> docker logs -f swk-challenge-php

Once it starts this [localhost:795](http://localhost:795) should display `Lumen (8.2.1) (Laravel Components ^8.0)`.

# Introduction

Calendar manager was built from the ground-up to make it easy for developers and users to access the fields for each day.
These README describe how to use the [Calendar Manager](https://https://gitlab.com/miguel.ptq/calendar-manager) API. We hope you enjoy these docs, and please don't hesitate to [file an issue](https://gitlab.com/miguel.ptq/calendar-manager/-/issues/new) if you see anything missing/wrong.

## Small summary of why I created this API

The objective of this API is to manage a calendar, creating fields for each day, editing the fields( `date`, name of the `field`) of a field when receiving an `Id`, show a field when receiving the `Id` of a field, show the list of all fields, this list is paginated, to paginate the way user wants, there are some fields that can be filled, page number, number of fields per page, the default value of the page number is 1 and the default fields per page is 10, these list can filtered as well, the way that the user wants to sort (`date`, `field` name), the direction (asc, desc), the default order value is the Id, and the direction is descendent, it's possible to delete a field, when given the `Id` of the field. 

<br />

# Errors

Field manager  returns the following status code in its API:

| Status Code | Description |
| :--- | :--- |
| 200 | `Ok` |
| 201 | `Created` |
| 400 | `Invalid Id supplied` |
| 404 | `Field Not found` |
| 422 | `Unprocessable Entity` |
| 405 | `Method not allowed` |

<br />

# Endpoints

## Show Field

* **URL**

    /fields/{id}

* **Method**

    `GET`

* **URL Parameters**

     **Required:**

     `id=[integer]`

* **Data Parameters**

    None

* **Success Response:**

````json
{
    "status": "success",
    "data" :
    {
        "field_id":1,
        "field": "test",
        "date": "1615415777"
    }
        
}
````
* **Error Response**

````json
{
    "status": "error",
    "response" :
        "code":400,
        "message": "Invalid ID"
}
````

OR

* **Error Response**

````json
{
    "status": "error",
    "response" :
        "code":404,
        "message": "Field not found"
}
````

## Show all fields and filter it

* **URL**

    /fields

* **Method**

    `GET`

* **URL Parameters**

     **Optional:**

     `pageSize=[integer]`<br/>
     `direction=[string]`<br/>
     `sortedBy=[string]`<br/>
     `field=[string]`
* **Data Parameters**

    None

* **Success Response:**

````json
{
    "status": "success",
    "data" :[
        {
            "field_id":1,
            "field": "test",
            "date": "1615415777"
        }
    ],
    "meta": {
        "total": 2,
        "perPage":10,
        "currentPage":5,
        "lastPage":5,
        "firstPage":1,
        "links": {
            "nextPageUrl":null,
            "prevPageUrl": "http://localhost/fields?page=4",
            "firstPageUrl": "http://localhost/fields?page=1",
            "lastPageUrl": "http://localhost/fields?page=5",
            "path": "http://localhost/fields"
        }
    }
}
````
* **Error Response**

````json
{
    "status": "Error"
    "response":
        "code":422
        "message": "Invalid or missing Parameters"
}
````

## Create field

* **URL**

    /fields

* **Method**

    `POST`

* **URL Parameters**

     None

* **Data Parameters**

    **Required**

    `field=[string]`

    **Optional**

     `date=[int]`

* **Success Response:**

````json
{
    "status": "success",
    "response" :
    {
        "field_id":1,
        "field": "test",
        "date": 1615415777
    }   
}
````

* **Error Response**
````json
{
    "status": "error",
    "response" :
        "code":422,
        "message": "Invalid or missing parameters"
}
````

OR

* **Error Response**

````json
{
    "status": "error",
   "response" :
        "code":405,
        "message": "Method not allowed"
}
````
     
## Delete field

* **URL**

    /fields/{id}

* **Method**

    `DELETE`

* **URL Parameters**

     **Required:**

     `id=[integer]`

* **Data Parameters**

    None

* **Success Response:**

````json
{
    "status": "success"
}
````

* **Error Response**

````json
{
    "status": "error",
    "response" :
        "code":400,
        "message": "Invalid ID supplied"
}
````

OR

* **Error Response**

````json
{
    "status": "error",
    "response" :
        "code":404,
        "message": "Field not found"
}
````

## Update field

* **URL**

    /fields/{id}

* **Method**

    `PUT`

* **URL Parameters**

     **Required:**

     `id=[integer]`

* **Data Parameters**

    **Optional**

    `field=[string]`<br/>
    `date=[integer]`

* **Success Response:**

````json
{
    "status": "success",
    "data" :
    {
        "field_id":7,
        "field": "title sample",
        "date": 1615415777
    }
        
}
````

* **Error Response**

````json
{
    "status": "error",
    "response" :
        "code":400,
        "message": "Invalid ID supplied"
}
````

OR

* **Error Response**

````json
{
    "status": "error",
    "response" :
        "code":404,
        "message": "Field not found"
}
````

<br />

# Tips

## Docker on windows

In windows you should have your projects inside the WSL environment to do this you can open the CMD and type:

> wsl

Then you will enter WSL environment, navigate to you user dir and created a dir named `projects`:

> cd ~
>
> mkdir projects
>
> cd projects

Now you can clone this repo to your dir:

> git clone git@gitlab.com:example-repo.git

## How to use `php artisan`

To use php artisan you must be in the container command line the the base directory of your lumen app,  to do that run the following command:

> docker exec -it swk-challenge-php bash

📝 Note: Now that you are in the container command line you should be able to use `php artisan`